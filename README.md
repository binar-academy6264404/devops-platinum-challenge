
# Devops Platinum Challenge

- Nama : Fathana Erlangga
- Wave : 2
- Pengajar: Mochammad Syaifuddin Khasani




## Langkah Pengerjaan Challenge
- Membuat Dockerfile dan Docker Images
- Membuat Kubernetes Cluster di Google Cloud Platform
- Membuat manifest kubernetes
- Mengimplementasikan Domain dan SSL 
- Membuat Gitlab runner pada Kubernetes Cluster
- Melakukan Deployment pada Kubernetes Cluster menggunakan Gitlab CI/CD
- Membuat dasboard monitoring dan Logging
- Menginstall Gitlab runner dengan Ansible
- Membuat VM, Gitlab Runner, dan CLoud SQL dengan Terraform



## Rancangan Infrastruktur 

![Rancangan Infra](/image/topologi.png)

## Membuat Dockerfile dan Docker Image
1. Buat Dockerfile di masing-masing folder backend dan Frontend
2. Kemudian build images dengan DOckerfile tersebut
3. Push image yang sudah dibikin tadi ke registry Docker yang sudah dimiliki sebelumnya

## Membuat Kubernetes CLuster
1. Buka menu Kubernetes Engine pada GCP
2. Kemudian klik Create untuk membuat awal Cluster
3. Isi nama cluster beserta Region yang di inginkan, serta setting juga node pada bagian tab kiri untuk menentukan node yang akan dibikin di Kubernetes Cluster
4. Jangan lupa untuk enable VM spot untuk mendapatkan harga yang lebih murah daripada VM standard 
5. Klik tombol Create dan tunggu sekitar 15 menit untuk membuat cluster pada GCP

## Membuat Manifest Kubernetes
1. Ada beberapa manifest yang dibutuhkan untuk mendeploy sebuah project kedalam Kubernetes
2. Yang harus kita buat diantaranya adalah Deployment,Ingress, Service, Namespace, Secret, COnfigmap, Certificate

## Mengimplementasikan Domain dan SSL
1. Buat IP static pada menu VPC di GCP
2. Buat 2 IP static untuk staging dan production
3. Setelah dibuat, kemudian tambahkan config annotation pada manifest Ingress 
4. Kemudian Menambahkan domain yang sudah dibuat kedalam manifest Certificate.yml
5. Lakukan hal yang sama pada Staging dan Prod

## Melakukan Deployment pada Kubernetes Cluster menggunakan Gitlab CI/CD
1. Dalam script .gitlab-ci.yaml terdapat 2 stage yaitu build dan deploy
2. Stage build bertugas untuk melakukan build image yang nantinya akan digunakan lalu kemudian melakukan push ke registry yang sudah ditentukan
3. Stage deploy bertugas untuk melakukan proses deployment kedalam server Staging dan Production
4. Setting beberapa variable kedalam Setting > CI/CD > Variable
5. Kemudian push code ke dalam server dengan melakukan commit
6. Setelah melakukan COmmit, hal tersebut akan langsung membuat triger CI/CD piplein berjalan
7. Proses tersebut bisa dipantau melalui tab Pipeline di samping kanan pada dashboard Gitlab

## Membuat dasboard monitoring dan Logging
